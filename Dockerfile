FROM openjdk:8-alpine
COPY target/config-client*.jar config-client.jar
CMD ["java", "-jar", "config-client.jar"]
